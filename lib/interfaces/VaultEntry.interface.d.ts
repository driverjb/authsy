export interface VaultEntry {
    id: number;
    passwordHash: string;
    salt: string;
}
