export declare const cannotDelete = "User id does not exist. Cannot delete entry.";
export declare const cannotUpdate = "User id does not exist. Cannot update password hash.";
export declare const multiChangeError: (total: number, action: string) => string;
export declare const failedCreate = "Failed to create new user entry";
declare const _default: {
    cannotDelete: string;
    cannotUpdate: string;
    multiChangeError: (total: number, action: string) => string;
    failedCreate: string;
};
export default _default;
