export interface HashResult {
  passwordHash: string;
  salt: string;
}
